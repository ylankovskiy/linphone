package org.linphone;

import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore.Images.Media;
//import android.support.v4.app.Fragment;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SpeedDialFragment extends Fragment {
    private static final int MAX_COUNT = 9;
    private static final String PREF_SD_IMAGE = "pref_sd_image_";
    private static final String PREF_SD_TEXT = "pref_sd_text_";
    private static final int RESULT_SELECT_GALLERY = 10001;
    private static final int RESULT_TAKE_PHOTO = 10000;
    private int currentIndex;
    private SpeedDialItem[] items;
    private String packageName;
    private Resources res;
    private SharedPreferences settings;

    public static String getPrefSdText() {
        return PREF_SD_TEXT;
    }

    /* renamed from: org.linphone.SpeedDialFragment.1 */
    class C02151 implements OnClickListener {
        final /* synthetic */ int val$index;

        C02151(int i) {
            this.val$index = i;
        }

        public void onClick(View v) {
            if (SpeedDialFragment.this.items[this.val$index].data == null) {
                SpeedDialFragment.this.showMenu(this.val$index);
            } else {
                LinphoneActivity.instance().setAddresGoToDialerAndCall(SpeedDialFragment.this.items[this.val$index].data, SpeedDialFragment.this.items[this.val$index].data, null);
            }
        }
    }

    /* renamed from: org.linphone.SpeedDialFragment.2 */
    class C02162 implements OnLongClickListener {
        final /* synthetic */ int val$index;

        C02162(int i) {
            this.val$index = i;
        }

        public boolean onLongClick(View v) {
            SpeedDialFragment.this.showMenu(this.val$index);
            return true;
        }
    }

    /* renamed from: org.linphone.SpeedDialFragment.3 */
    class C02173 implements OnItemClickListener {
        final /* synthetic */ int val$index;

        C02173(int i) {
            this.val$index = i;
        }

        public void onItemClick(AdapterView<?> adapterView, View arg1, int item, long arg3) {
            if (item == 0) {
                SpeedDialFragment.this.updateDialNumber(this.val$index);
            } else if (item == 1) {
                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                intent.putExtra("output", Uri.fromFile(SpeedDialFragment.this.getFile("temp.jpg")));
                SpeedDialFragment.this.startActivityForResult(intent, SpeedDialFragment.RESULT_TAKE_PHOTO);
            } else if (item == 2) {
                SpeedDialFragment.this.startActivityForResult(new Intent("android.intent.action.PICK", Media.EXTERNAL_CONTENT_URI), SpeedDialFragment.RESULT_SELECT_GALLERY);
            } else if (item == 3) {
                if (SpeedDialFragment.this.items[this.val$index].path != null) {
                    new File(SpeedDialFragment.this.items[this.val$index].path).delete();
                    SpeedDialFragment.this.items[this.val$index].path = null;
                }
                SpeedDialFragment.this.items[this.val$index].data = null;
                SpeedDialFragment.this.saveItem(this.val$index);
                SpeedDialFragment.this.updateViews(this.val$index);
            }
        }
    }

    /* renamed from: org.linphone.SpeedDialFragment.4 */
    class C02184 implements DialogInterface.OnClickListener {
        C02184() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    /* renamed from: org.linphone.SpeedDialFragment.5 */
    class C02195 implements DialogInterface.OnClickListener {
        final /* synthetic */ int val$index;
        final /* synthetic */ EditText val$num;

        C02195(EditText editText, int i) {
            this.val$num = editText;
            this.val$index = i;
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            String data = this.val$num.getText().toString().trim();
            if (data == null || data.trim().length() == 0) {
                data = null;
            }
            SpeedDialFragment.this.items[this.val$index].data = data;
            SpeedDialFragment.this.updateViews(this.val$index);
            SpeedDialFragment.this.saveItem(this.val$index);
        }
    }

    /* renamed from: org.linphone.SpeedDialFragment.6 */
    class C02206 implements DialogInterface.OnClickListener {
        C02206() {
        }

        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    private class SpeedDialItem {
        String data;
        ImageView image;
        LinearLayout layout;
        String path;
        TextView text;

        private SpeedDialItem() {
        }
    }

    public SpeedDialFragment() {
        this.items = new SpeedDialItem[MAX_COUNT];
        this.currentIndex = -1;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int i;
        View rootView = inflater.inflate(R.layout.fragment_speed_dial, container, false);
        this.res = getActivity().getResources();
        this.packageName = "org.linphone";
        Context context = getActivity();
        this.settings = context.getSharedPreferences(
                getString(R.string.speed_dial_preference_key), Context.MODE_PRIVATE);
        //PreferenceManager.getDefaultSharedPreferences(getActivity());
        for (i = 0; i < MAX_COUNT; i++) {
            this.items[i] = getItem(inflater, rootView, i);
        }
        for (i = 0; i < MAX_COUNT; i++) {
            updateViews(i);
            setListener(i);
        }
        return rootView;
    }

    private int getResId(String type, int index) {
        return this.res.getIdentifier(type + index, "id", this.packageName);
    }

    private SpeedDialItem getItem(LayoutInflater inflater, View rootView, int index) {
        SpeedDialItem item = new SpeedDialItem();
        View layout = inflater.inflate(R.layout.speed_dial_item, (FrameLayout) rootView.findViewById(getResId("sd_frame_", index)), true);
        item.layout = (LinearLayout) layout.findViewById(R.id.sd_layout);
        int id = getResId("sd_image_", index);
        item.image = (ImageView) layout.findViewById(R.id.sd_image);
        id = getResId("sd_text_", index);
        item.text = (TextView) layout.findViewById(R.id.sd_text);
        item.path = this.settings.getString(PREF_SD_IMAGE + index, null);
        item.data = this.settings.getString(PREF_SD_TEXT + index, null);

        return item;
    }

    private void updateViews(int index) {
        SpeedDialItem item = this.items[index];
        if (item.path == null) {
            item.image.setImageResource(R.drawable.unknown_small);
        } else {
            File imgFile = getFile(item.path);
            if (imgFile.exists()) {
                item.image.setImageBitmap(BitmapFactory.decodeFile(imgFile.getAbsolutePath()));
            }
        }
        if (item.data == null) {
            item.text.setText(R.string.empty_string);
        } else {
            item.text.setText(item.data);
        }
    }

    private void setListener(int index) {
        this.items[index].layout.setOnClickListener(new C02151(index));
        this.items[index].layout.setOnLongClickListener(new C02162(index));
    }

    private void showMenu(int index) {
        Builder builder = new Builder(getActivity());
        builder.setTitle(R.string.options_select);
        this.currentIndex = index;
        ListView list = new ListView(getActivity());
        list.setAdapter(new ArrayAdapter(getActivity(), 17367043, this.res.getStringArray(R.array.options_array)));
        builder.setView(list);
        list.setOnItemClickListener(new C02173(index));
        builder.setPositiveButton(17039370, new C02184());
        builder.show();
    }

    private void updateDialNumber(int index) {
        Builder builder = new Builder(getActivity());
        builder.setTitle(R.string.title_spedddial_dialnumber);
        builder.setMessage(R.string.message_spedddial_dialnumber);
        EditText num = new EditText(getActivity());
        if (this.items[index].data != null) {
            num.setText(this.items[index].data);
        }
        builder.setView(num);
        builder.setPositiveButton(17039370, new C02195(num, index));
        builder.setNegativeButton(17039360, new C02206());
        builder.create().show();
    }

    private Bitmap getBitmap(String filePath, int index) {
        int targetW = this.items[index].image.getWidth();
        int targetH = this.items[index].image.getHeight();
        Options bmOptions = new Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, bmOptions);
        int scaleFactor = Math.min(bmOptions.outWidth / targetW, bmOptions.outHeight / targetH);
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        return BitmapFactory.decodeFile(filePath, bmOptions);
    }

    private void saveFileThumbnail(String path, int index) {
        Bitmap bmp = getBitmap(path, index);
        try {
            FileOutputStream fOut = new FileOutputStream(getFile("sb_image_" + index + ".png"));
            bmp.compress(CompressFormat.PNG, 85, fOut);
            fOut.flush();
            fOut.close();
            this.items[index].path = "sb_image_" + index + ".png";
            saveItem(index);
            updateViews(index);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == -1) {
            if (requestCode == RESULT_SELECT_GALLERY) {
                String[] filePath = new String[]{"_data"};
                Cursor c = getActivity().getContentResolver().query(data.getData(), filePath, null, null, null);
                c.moveToFirst();
                saveFileThumbnail(c.getString(c.getColumnIndex(filePath[0])), this.currentIndex);
            } else if (requestCode == RESULT_TAKE_PHOTO) {
                saveFileThumbnail(getFile("temp.jpg").getAbsolutePath(), this.currentIndex);
                getFile("temp.jpg").delete();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void saveItem(int index) {
        Editor editor = this.settings.edit();
        editor.putString(PREF_SD_IMAGE + index, this.items[index].path);
        editor.putString(PREF_SD_TEXT + index, this.items[index].data);
        editor.commit();
    }

    private File getFile(String filename) {
        File dir = new File(Environment.getExternalStorageDirectory() + "/mialert");
        if (!dir.exists()) {
            dir.mkdir();
        }
        return new File(dir, filename);
    }
}
