package org.linphone;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.linphone.LinphoneUtils;
import java.net.HttpURLConnection;
import java.io.IOException;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.StringBuilder;
import java.io.BufferedReader;
import java.net.URL;
import org.json.JSONObject;
import org.json.JSONArray;
import org.linphone.JSONParser;
import org.json.JSONException;
import android.widget.Toast;
import java.util.HashMap;
import android.os.AsyncTask;
import android.app.ProgressDialog;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Button;
import java.util.ArrayList;
import android.graphics.Point;
import android.app.Activity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.LinearLayout;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.View.OnClickListener;

import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Bitmap;
import android.content.Intent;
import android.widget.BaseAdapter;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.graphics.BitmapFactory;
import java.io.ByteArrayOutputStream;
import android.util.Base64;
import java.lang.Thread;
import android.os.Build;

/**
 * @author Yury Lankovskiy
 */
public class ProvisioningEMSFragment extends Fragment {

    private static String actionCreatePatient = "CREATEPATIENT";
    private static String actionUpdatePatient = "UPDATEPATIENT";
    private static String actionDeletePatient = "DELETEPATIENT";

    private static String actionCreateDevice= "CREATEDEVICE";
    private static String actionUpdateDevice = "UPDATEDEVICE";
    private static String actionDeleteDevice = "DELETEDEVICE";

    private static String actionListFloor = "LISTFLOOR";
    private static String actionListRoom = "LISTROOM";
    private static String actionListRoomInfo = "ROOMINFO";

    private static String WEB_API_URL = "http://173.231.102.88/webapi";
    private static String WEB_API_METHOD = "POST";

    private static String UNIQUE_ID = null;

    private static ProgressDialog pDialog = null;

    private static Button buttonRefreshView = null;
    private static Button buttonAddDevice = null;
    private static Button buttonAddPatient = null;

    private ArrayList<Building> buildingList = null;
    private ArrayList<Floor> floorList = null;
    private ArrayList<Room> roomList = null;
    private ArrayList<Patient> patientList = null;
    private ArrayList<Device> deviceList = null;

    private ArrayList<String> buildingAdapter = null;
    private ArrayList<String> floorAdapter = null;
    private ArrayList<String> roomAdapter = null;
    private ArrayList<String> deviceAdapter = null;
    private ArrayList<String> patientAdapter = null;

    private static Spinner spinnerBuilding = null;
    private static Spinner spinnerFloor= null;
    private static Spinner spinnerRoom = null;
    private static Spinner spinnerDevice = null;
    private static Spinner spinnerPatient = null;

    private static Point p;

    private static View view;

    private static EditText popupDeviceName = null;
    private static EditText popupPatientFirstName = null;
    private static EditText popupPatientLastName = null;
    private static EditText popupPatientAffliction = null;

    private static TextView buildingLabel = null;
    private static TextView floorLabel = null;
    private static TextView roomLabel = null;
    private static TextView deviceLabel = null;
    private static TextView patientLabel = null;

    private static int popupWidth = 0;
    private static int popupHeight = 0;
    private static int popupUserImageWidth = 0;
    private static int popupUserImageHeight = 0;
    private static int OFFSET_X = 50;
    private static int OFFSET_Y = 50;

    private static boolean FLAG_SET_IMAGE_VIEW = false;

    private class MyDevice {
        String mac_address;
        String ip_address;
        String device_type = "miDome";

        MyDevice() {
            this.mac_address = LinphoneUtils.getMacAddress();
            this.ip_address = LinphoneUtils.getIpAddress();
        }

        public String getMacAddress() {
            return this.mac_address;
        }

        public String getIpAddress() {
            return this.ip_address;
        }

        public String getDeviceType() {
            return this.device_type;
        }
    }

    private class Building {
        int id;
        String name;

        Building() {

        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }
    }

    private class Floor {
        int id;
        String name;

        Floor() {

        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }
    }

    private class Room {
        int id;
        String name;

        Room() {

        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }
    }

    private class Device {
        int id;
        String name;
        String action;

        Device() {

        }

        public void setId(int id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setAction(String action) { this.action = action; }

        public int getId() {
            return this.id;
        }

        public String getName() {
            return this.name;
        }

        public String getAction() { return this.action; }
    }

    private class Patient {
        int id;
        String first_name;
        String last_name;
        String affliction;
        String image_url;
        String action;
        String image_base64;

        Patient() {

        }

        public void setId(int id) {
            this.id = id;
        }

        public void setFirstName(String first_name) {
            this.first_name = first_name;
        }

        public void setLastName(String last_name) {
            this.last_name = last_name;
        }

        public void setAffliction(String affliction) {
            this.affliction = affliction;
        }

        public void setImageUrl(String image_url) {
            this.image_url = image_url;
        }

        public void setImageBase64(String image_base64) {
            this.image_base64 = image_base64;
        }

        public void setAction(String action) { this.action = action; }

        public int getId() {
            return this.id;
        }

        public String getFirstName() {
            return this.first_name;
        }

        public String getLastName() {
            return this.last_name;
        }

        public String getAffliction() {
            return this.affliction;
        }

        public String getImageUrl() {
            return this.image_url;
        }

        public String getImageBase64() {
            return this.image_base64;
        }

        public String getAction() { return this.action; }
    }

    // convert InputStream to String
    private String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return sb.toString();
    }

    private void showProgressDialog(String message) {
        if (pDialog == null) {
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage(message);
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
        }
        pDialog.show();
    }

    private void dismissProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        dismissProgressDialog();
        super.onDestroy();
    }

    private void redrawEverything(TableLayout tableLayout) {
        tableLayout.invalidate();
        tableLayout.refreshDrawableState();
    }

    // The method that displays the popup.
    private void showPopupDevice(final Activity context, Point p, final Device device) {
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.provisioning_ems_popup_device);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(getActivity().getApplicationContext().LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.provisioning_ems_device, viewGroup);

        popupDeviceName = (EditText) layout.findViewById(R.id.provisioning_ems_device_name);

        if (device != null) {
            popupDeviceName.setText(device.getName());
        }

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, OFFSET_X, OFFSET_Y);

        // Getting a reference to Close button, and close the popup when clicked.
        Button close = (Button) layout.findViewById(R.id.provisioning_ems_popup_device_close);
        close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        Button delete = (Button) layout.findViewById(R.id.provisioning_ems_popup_device_delete);
        if (device != null) {
            delete.setVisibility(View.VISIBLE);
            delete.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (device.getId() > 0) {
                        device.setAction(actionDeleteDevice);
//                        new RoomDevicePostAsync().execute(device);

                        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                            new RoomDevicePostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, device);
                        else
                            new RoomDevicePostAsync().execute(device);

                        popup.dismiss();
                    } else {
                        Toast.makeText(getActivity(), "Error deleting a device that has not been created",
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            delete.setVisibility(View.GONE);
        }

        Button save = (Button) layout.findViewById(R.id.provisioning_ems_popup_device_save);
        save.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (popupDeviceName.getText().toString().length() > 0) {
                    if (device != null) {
                        device.setAction(actionUpdateDevice);
                        device.setName(popupDeviceName.getText().toString());
//                        new RoomDevicePostAsync().execute(device);

                        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                            new RoomDevicePostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, device);
                        else
                            new RoomDevicePostAsync().execute(device);
                    } else {
                        Device deviceTemp = new Device();

                        deviceTemp.setId(0);
                        deviceTemp.setName(popupDeviceName.getText().toString());
                        deviceTemp.setAction(actionCreateDevice);

//                        new RoomDevicePostAsync().execute(deviceTemp);

                        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                            new RoomDevicePostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, deviceTemp);
                        else
                            new RoomDevicePostAsync().execute(deviceTemp);
                    }
                    popup.dismiss();
                } else {
                    Toast.makeText(getActivity(), "Please enter the device information",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private static final int CAMERA_REQUEST = 1888;
    private ImageView patientImageView;

    private void takeImageFromCamera(View view) {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            Bitmap mphoto = (Bitmap) data.getExtras().get("data");
            patientImageView.setImageBitmap(mphoto);

            FLAG_SET_IMAGE_VIEW = true;
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
//            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            if (result != null)
                bmImage.setImageBitmap(result);
        }
    }

    private void showPopupPatient(final Activity context, Point p, final Patient patient) {
        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) getActivity().findViewById(R.id.provisioning_ems_popup_patient);
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getApplicationContext().getSystemService(getActivity().getApplicationContext().LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.provisioning_ems_patient, viewGroup);

        FLAG_SET_IMAGE_VIEW = false;

        popupPatientFirstName = (EditText) layout.findViewById(R.id.provisioning_ems_patient_first_name);
        popupPatientLastName = (EditText) layout.findViewById(R.id.provisioning_ems_patient_last_name);
        popupPatientAffliction = (EditText) layout.findViewById(R.id.provisioning_ems_patient_affliction);

        patientImageView = (ImageView) layout.findViewById(R.id.provisioning_ems_patient_image_key);
        patientImageView.getLayoutParams().height = popupUserImageHeight;
        patientImageView.getLayoutParams().width = popupUserImageWidth;
        patientImageView.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                takeImageFromCamera(layout);
            }
        });

        if (patient != null) {
            popupPatientFirstName.setText(patient.getFirstName());
            popupPatientLastName.setText(patient.getLastName());
            popupPatientAffliction.setText(patient.getAffliction());

            if (patient.getImageUrl() != null) {
//                new DownloadImageTask(patientImageView).execute(patient.getImageUrl());

                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                    new DownloadImageTask(patientImageView).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, patient.getImageUrl());
                else
                    new DownloadImageTask(patientImageView).execute(patient.getImageUrl());
            }
        }

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth(popupWidth);
        popup.setHeight(popupHeight);
        popup.setFocusable(true);

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, OFFSET_X, OFFSET_Y);

        // Getting a reference to Close button, and close the popup when clicked.
        Button close = (Button) layout.findViewById(R.id.provisioning_ems_popup_patient_close);
        close.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        Button delete = (Button) layout.findViewById(R.id.provisioning_ems_popup_patient_delete);
        if (patient != null) {
            delete.setVisibility(View.VISIBLE);
            delete.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    if (patient.getId() > 0) {
                        patient.setAction(actionDeletePatient);
//                        new RoomPatientPostAsync().execute(patient);

                        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                            new RoomPatientPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, patient);
                        else
                            new RoomPatientPostAsync().execute(patient);

                        popup.dismiss();
                    } else {
                        Toast.makeText(getActivity(), "Error deleting a patient that has not been created",
                                Toast.LENGTH_LONG).show();
                    }
                }
            });
        } else {
            delete.setVisibility(View.GONE);
        }

        Button save = (Button) layout.findViewById(R.id.provisioning_ems_popup_patient_save);
        save.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (popupPatientFirstName.getText().toString().length() > 0 &&
                        popupPatientLastName.getText().toString().length() > 0 &&
                        popupPatientAffliction.getText().toString().length() > 0) {
                    if (patient != null) {
                        patient.setAction(actionUpdatePatient);
                        patient.setFirstName(popupPatientFirstName.getText().toString());
                        patient.setLastName(popupPatientLastName.getText().toString());
                        patient.setAffliction(popupPatientAffliction.getText().toString());

                        if (FLAG_SET_IMAGE_VIEW) {
                        /*
                        *   Convert image in ImageView to base 64 and store to object
                         */
                            BitmapDrawable drawable = (BitmapDrawable) patientImageView.getDrawable();
                            Bitmap bitmap = drawable.getBitmap();
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                            byte[] image = stream.toByteArray();
                            patient.setImageBase64(Base64.encodeToString(image, 0));
                        }

//                        new RoomPatientPostAsync().execute(patient);

                        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                            new RoomPatientPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, patient);
                        else
                            new RoomPatientPostAsync().execute(patient);
                    } else {
                        Patient patientTemp = new Patient();

                        patientTemp.setId(0);
                        patientTemp.setFirstName(popupPatientFirstName.getText().toString());
                        patientTemp.setLastName(popupPatientLastName.getText().toString());
                        patientTemp.setAffliction(popupPatientAffliction.getText().toString());
                        patientTemp.setAction(actionCreatePatient);

                        if (FLAG_SET_IMAGE_VIEW) {
                        /*
                        *   Convert image in ImageView to base 64 and store to object
                         */
                            BitmapDrawable drawable = (BitmapDrawable) patientImageView.getDrawable();
                            Bitmap bitmap = drawable.getBitmap();
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                            byte[] image = stream.toByteArray();
                            patient.setImageBase64(Base64.encodeToString(image, 0));
                        }

//                        new RoomPatientPostAsync().execute(patientTemp);

                        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                            new RoomPatientPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, patientTemp);
                        else
                            new RoomPatientPostAsync().execute(patientTemp);
                    }
                    popup.dismiss();
                } else {
                    Toast.makeText(getActivity(), "Please fill in the patient information",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private class RoomPatientPostAsync extends AsyncTask<Patient, Void, JSONObject> {
        JSONParser jsonParser = new JSONParser();

        private ProgressDialog pDialog;

//        private static final String TAG_ROOM = "rooms";
//        private static final String TAG_UNIC_ID = "unic_id";


        @Override
        protected void onPreExecute() {
            showProgressDialog("Loading. Please wait...");
        }

        @Override
        protected JSONObject doInBackground(Patient... arg) {
//            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

            try {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("unic_id", UNIQUE_ID);
                params.put("action", arg[0].getAction());

                if (arg[0].getAction().equals(actionCreatePatient)) {
//                  { unic_id:4b3403665fea6, action:’CREATEPATIENT’,first_name:’First’, last_name:’Last’, id_room:1, affliction:’Affliction’, photo:’images’}
                    params.put("first_name", arg[0].getFirstName());
                    params.put("last_name", arg[0].getLastName());
                    params.put("affliction", arg[0].getAffliction());
                    if (arg[0].getImageBase64() != null)
                        params.put("photo", arg[0].getImageBase64());
                    params.put("id_room", String.valueOf(roomList.get(spinnerRoom.getSelectedItemPosition()).getId()));
                } else if (arg[0].getAction().equals(actionUpdatePatient)) {
//                  { unic_id:4b3403665fea6, action:’UPDATEPATIENT’,id_patient:1, first_name:’First’, last_name:’Last’, affliction:’Affliction’, photo:’images’}
                    params.put("first_name", arg[0].getFirstName());
                    params.put("last_name", arg[0].getLastName());
                    params.put("affliction", arg[0].getAffliction());
                    if (arg[0].getImageBase64() != null)
                        params.put("photo", arg[0].getImageBase64());
                    params.put("id_patient", String.valueOf(arg[0].getId()));
                } else if (arg[0].getAction().equals(actionDeletePatient)) {
//                  { unic_id:4b3403665fea6, action:’DELETEPATIENT’,id_patient:1}
                    params.put("id_patient", String.valueOf(arg[0].getId()));
                }


                JSONObject json = jsonParser.makeHttpRequest(
                        WEB_API_URL, WEB_API_METHOD, params);

                if (json != null) {
                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {
            if (getActivity().isDestroyed()) {
                return;
            }
            dismissProgressDialog();

            if (json != null) {
//                Toast.makeText(getActivity(), json.toString(),
//                        Toast.LENGTH_LONG).show();

//                new RoomInfoPostAsync().execute(roomList.get(spinnerRoom.getSelectedItemPosition()));

                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                    new RoomInfoPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, roomList.get(spinnerRoom.getSelectedItemPosition()));
                else
                    new RoomInfoPostAsync().execute(roomList.get(spinnerRoom.getSelectedItemPosition()));
            } else {
                Toast.makeText(getActivity(), "NULL RESPONSE",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    private class RoomDevicePostAsync extends AsyncTask<Device, Void, JSONObject> {
        JSONParser jsonParser = new JSONParser();

        private ProgressDialog pDialog;

//        private static final String TAG_ROOM = "rooms";
//        private static final String TAG_UNIC_ID = "unic_id";


        @Override
        protected void onPreExecute() {
            showProgressDialog("Loading. Please wait...");
        }

        @Override
        protected JSONObject doInBackground(Device... arg) {
//            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

            try {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("unic_id", UNIQUE_ID);
                params.put("action", arg[0].getAction());

                if (arg[0].getAction().equals(actionCreateDevice)) {
//                  { unic_id:4b3403665fea6, action:’CREATEDEVICE’, baseName:’6305F4’, baseType:’miDome’, id_room:1}
                    params.put("baseName", LinphoneUtils.getDeviceSerial());
                    params.put("baseType", LinphoneUtils.getDeviceType());
                    params.put("id_room", String.valueOf(roomList.get(spinnerRoom.getSelectedItemPosition()).getId()));
                } else if (arg[0].getAction().equals(actionUpdateDevice)) {
//                  { unic_id:4b3403665fea6, action:’UPDATEDEVICE’, id_base:1, baseName:’6305F4 Modified’, baseType:’miDome’ }
                    params.put("id_base", String.valueOf(arg[0].getId()));
                    params.put("baseName", LinphoneUtils.getDeviceSerial());
                    params.put("baseType", LinphoneUtils.getDeviceType());
                } else if (arg[0].getAction().equals(actionDeleteDevice)) {
//                  { unic_id:4b3403665fea6, action:’DELETEDEVICE’, id_base:1 }
                    params.put("id_base", String.valueOf(arg[0].getId()));
                }

                JSONObject json = jsonParser.makeHttpRequest(
                        WEB_API_URL, WEB_API_METHOD, params);

                if (json != null) {
                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {
            if (getActivity().isDestroyed()) {
                return;
            }
            dismissProgressDialog();

            if (json != null) {
//                Toast.makeText(getActivity(), json.toString(),
//                        Toast.LENGTH_LONG).show();

//                new RoomInfoPostAsync().execute(roomList.get(spinnerRoom.getSelectedItemPosition()));

                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                    new RoomInfoPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, roomList.get(spinnerRoom.getSelectedItemPosition()));
                else
                    new RoomInfoPostAsync().execute(roomList.get(spinnerRoom.getSelectedItemPosition()));
            } else {
                Toast.makeText(getActivity(), "NULL RESPONSE",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    private class RoomInfoPostAsync extends AsyncTask<Room, Void, JSONObject> {
        JSONParser jsonParser = new JSONParser();

        private ProgressDialog pDialog;

        private static final String TAG_DEVICE = "devices";
        private static final String TAG_PATIENT = "patients";
        private static final String TAG_UNIC_ID = "unic_id";


        @Override
        protected void onPreExecute() {
            showProgressDialog("Loading. Please wait...");
        }

        @Override
        protected JSONObject doInBackground(Room... arg) {
//            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

            try {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("unic_id", UNIQUE_ID);
                params.put("action", actionListRoomInfo);
                params.put("id_room", String.valueOf(arg[0].getId()));

                JSONObject json = jsonParser.makeHttpRequest(
                        WEB_API_URL, WEB_API_METHOD, params);

                if (json != null) {
                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {
            if (getActivity().isDestroyed()) {
                return;
            }
            dismissProgressDialog();

            JSONArray deviceArray = null;
            JSONArray patientArray = null;

            deviceList = new ArrayList<Device>();
            deviceAdapter = new ArrayList<String>();

            patientList = new ArrayList<Patient>();
            patientAdapter = new ArrayList<String>();

            if (json != null) {
//                Toast.makeText(getActivity(), json.toString(),
//                        Toast.LENGTH_LONG).show();
                clearPastRoomSpinnerData();
                hidePastRoomContent();

                if (deviceLabel != null)
                    deviceLabel.setVisibility(View.VISIBLE);
                if (patientLabel != null)
                    patientLabel.setVisibility(View.VISIBLE);
                if (spinnerDevice != null)
                    spinnerDevice.setVisibility(View.VISIBLE);
                if (spinnerPatient != null)
                    spinnerPatient.setVisibility(View.VISIBLE);

                buttonAddDevice.setVisibility(View.VISIBLE);
                buttonAddPatient.setVisibility(View.VISIBLE);

                try {
                    // workaround, add empty device at the top of the list
                    Device deviceEmpty = new Device();

                    // add device to the list of devices
                    deviceEmpty.setId(0);
                    deviceEmpty.setName("Select room...");
                    deviceList.add(deviceEmpty);
                    // populate spinner with device names
                    deviceAdapter.add("Select device...");

                    // workaround, add empty patient at the top of the list
                    Patient patientEmpty = new Patient();

                    // add patient to the list of patients
                    patientEmpty.setId(0);
                    patientEmpty.setFirstName("Select room...");
                    patientEmpty.setLastName("Select room...");
                    patientEmpty.setAffliction("Select room...");
                    patientList.add(patientEmpty);
                    // populate spinner with patient names
                    patientAdapter.add("Select patient...");

                    deviceArray = json.getJSONArray(TAG_DEVICE);
                    patientArray = json.getJSONArray(TAG_PATIENT);
                    for (int i = 0; i < deviceArray.length(); i++) {
                        JSONObject jsonobject = deviceArray.getJSONObject(i);

                        Device device = new Device();

                        // add device to the list of devices
                        device.setId(jsonobject.getInt("id_device"));
                        device.setName(jsonobject.getString("name"));
                        deviceList.add(device);

                        // populate spinner with device names
                        deviceAdapter.add(jsonobject.getString("name"));
                    }

                    for (int i = 0; i < patientArray.length(); i++) {
                        JSONObject jsonobject = patientArray.getJSONObject(i);

                        Patient patient = new Patient();

                        // add patient to the list of patientss
                        patient.setId(jsonobject.getInt("id_patients"));
                        patient.setFirstName(jsonobject.getString("first_name"));
                        patient.setLastName(jsonobject.getString("last_name"));
                        patient.setAffliction(jsonobject.getString("affliction"));
                        patient.setImageUrl(jsonobject.getString("patients_photo"));
                        patientList.add(patient);

                        // populate spinner with patient names
                        patientAdapter.add(jsonobject.getString("first_name") + " " + jsonobject.getString("last_name"));
                    }

                    spinnerDevice.setAdapter(new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            deviceAdapter));
                    spinnerDevice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0,
                                                   View arg1, int position, long arg3) {
                            // TODO Auto-generated method stub
                            if (position == 0)
                                return;
//                            Toast.makeText(getActivity(), deviceList.get(position).getId() + " " + deviceList.get(position).getName(),
//                                    Toast.LENGTH_LONG).show();
                            showPopupDevice(getActivity(), p, deviceList.get(position));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub
                        }
                    });

                    spinnerPatient.setAdapter(new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            patientAdapter));
                    spinnerPatient.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0,
                                                   View arg1, int position, long arg3) {
                            // TODO Auto-generated method stub
                            if (position == 0)
                                return;
//                            Toast.makeText(getActivity(),
//                                    patientList.get(position).getId() + " " + patientList.get(position).getFirstName() + " " +
//                                    patientList.get(position).getLastName() + " " patientList.get(position).getAffliction(),
//                                    Toast.LENGTH_LONG).show();

                            showPopupPatient(getActivity(), p, patientList.get(position));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub
                        }
                    });


//                    StringBuilder sb = new StringBuilder();
//                    for (Device d : deviceList) {
//                        sb.append(d.getId());
//                        sb.append("\t");
//                        sb.append(d.getName());
//                        sb.append("\n");
//                    }
//
//                    Toast.makeText(getActivity(), "devices : " + sb.toString(),
//                            Toast.LENGTH_LONG).show();
//
//                    sb = new StringBuilder();
//                    for (Patient p : patientList) {
//                        sb.append(p.getId());
//                        sb.append("\t");
//                        sb.append(p.getFirstName());
//                        sb.append("\t");
//                        sb.append(p.getLastName());
//                        sb.append("\t");
//                        sb.append(p.getAffliction());
//                        sb.append("\t");
//                        sb.append(p.getImageUrl());
//                        sb.append("\n");
//                    }
//
//                    Toast.makeText(getActivity(), "patients : " + sb.toString(),
//                            Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(), "NULL RESPONSE",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    private class RoomPostAsync extends AsyncTask<Floor, Void, JSONObject> {
        JSONParser jsonParser = new JSONParser();

        private ProgressDialog pDialog;

        private static final String TAG_ROOM = "rooms";
        private static final String TAG_UNIC_ID = "unic_id";


        @Override
        protected void onPreExecute() {
            showProgressDialog("Loading. Please wait...");
        }

        @Override
        protected JSONObject doInBackground(Floor... arg) {
//            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

            try {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("unic_id", UNIQUE_ID);
                params.put("action", actionListRoom);
                params.put("id", String.valueOf(arg[0].getId()));


                JSONObject json = jsonParser.makeHttpRequest(
                        WEB_API_URL, WEB_API_METHOD, params);

                if (json != null) {
                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {
            if (getActivity().isDestroyed()) {
                return;
            }
            dismissProgressDialog();

            JSONArray roomArray = null;

            roomList = new ArrayList<Room>();
            roomAdapter = new ArrayList<String>();

            if (json != null) {
//                Toast.makeText(getActivity(), json.toString(),
//                        Toast.LENGTH_LONG).show();
                clearPastFloorSpinnerData();
                hidePastFloorContent();

                if (roomLabel != null)
                    roomLabel.setVisibility(View.VISIBLE);
                if (spinnerRoom != null)
                    spinnerRoom.setVisibility(View.VISIBLE);

                try {
                    // workaround, add empty room at the top of the list
                    Room room = new Room();

                    // add room to the list of rooms
                    room.setId(0);
                    room.setName("Select room...");
                    roomList.add(room);
                    // populate spinner with room names
                    roomAdapter.add("Select room...");

//                    UNIQUE_ID = json.getString(TAG_UNIC_ID);

                    roomArray = json.getJSONArray(TAG_ROOM);
                    for (int i = 0; i < roomArray.length(); i++) {
                        JSONObject jsonobject = roomArray.getJSONObject(i);

                        room = new Room();

                        // add room to the list of rooms
                        room.setId(jsonobject.getInt("id_room"));
                        room.setName(jsonobject.getString("name"));
                        roomList.add(room);

                        // populate spinner with room names
                        roomAdapter.add(jsonobject.getString("name"));
                    }

                    spinnerRoom.setAdapter(new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            roomAdapter));
                    spinnerRoom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0,
                                                   View arg1, int position, long arg3) {
                            // TODO Auto-generated method stub
                            if (position == 0)
                                return;
//                            Toast.makeText(getActivity(), roomList.get(position).getId() + " " + roomList.get(position).getName(),
//                                    Toast.LENGTH_LONG).show();

//                            new RoomInfoPostAsync().execute(roomList.get(position));

                            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                                new RoomInfoPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, roomList.get(position));
                            else
                                new RoomInfoPostAsync().execute(roomList.get(position));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(), "NULL RESPONSE",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    private class FloorPostAsync extends AsyncTask<Building, Void, JSONObject> {
        JSONParser jsonParser = new JSONParser();

        private static final String TAG_FLOOR = "floor";
        private static final String TAG_UNIC_ID = "unic_id";


        @Override
        protected void onPreExecute() {
            showProgressDialog("Loading. Please wait...");
        }

        @Override
        protected JSONObject doInBackground(Building... arg) {
//            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

            try {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("unic_id", UNIQUE_ID);
                params.put("action", actionListFloor);
                params.put("id", String.valueOf(arg[0].getId()));


                JSONObject json = jsonParser.makeHttpRequest(
                        WEB_API_URL, WEB_API_METHOD, params);

                if (json != null) {
                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {
            if (getActivity().isDestroyed()) {
                return;
            }
            dismissProgressDialog();

            JSONArray floorArray = null;

            floorList = new ArrayList<Floor>();
            floorAdapter = new ArrayList<String>();

            if (json != null) {
//                Toast.makeText(getActivity(), json.toString(),
//                        Toast.LENGTH_LONG).show();
                clearPastBuildingSpinnerData();
                hidePastBuildingContent();

                if (floorLabel != null)
                    floorLabel.setVisibility(View.VISIBLE);
                if (spinnerFloor != null)
                    spinnerFloor.setVisibility(View.VISIBLE);

                try {
                    // workaround, add empty floor at the top of the list
                    Floor floor = new Floor();

                    // add floor to the list of floors
                    floor.setId(0);
                    floor.setName("Select floor...");
                    floorList.add(floor);
                    // populate spinner with floor names
                    floorAdapter.add("Select floor...");

//                    UNIQUE_ID = json.getString(TAG_UNIC_ID);

                    floorArray = json.getJSONArray(TAG_FLOOR);
                    for (int i = 0; i < floorArray.length(); i++) {
                        JSONObject jsonobject = floorArray.getJSONObject(i);

                        floor = new Floor();

                        // add floor to the list of floors
                        floor.setId(jsonobject.getInt("id"));
                        floor.setName(jsonobject.getString("name"));
                        floorList.add(floor);

                        // populate spinner with floor names
                        floorAdapter.add(jsonobject.getString("name"));
                    }

                    spinnerFloor.setAdapter(new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item,
                            floorAdapter));
                    spinnerFloor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0,
                                                   View arg1, int position, long arg3) {
                            // TODO Auto-generated method stub
                            if (position == 0)
                                return;
//                            Toast.makeText(getActivity(), floorList.get(position).getId() + " " + floorList.get(position).getName(),
//                                    Toast.LENGTH_LONG).show();

//                            new RoomPostAsync().execute(floorList.get(position));

                            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                                new RoomPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, floorList.get(position));
                            else
                                new RoomPostAsync().execute(floorList.get(position));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(), "NULL RESPONSE",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    private class BuildingPostAsync extends AsyncTask<String, String, JSONObject> {
        JSONParser jsonParser = new JSONParser();

        private ProgressDialog pDialog;

        private static final String TAG_BUILDING = "building";
        private static final String TAG_UNIC_ID = "unic_id";


        @Override
        protected void onPreExecute() {
            showProgressDialog("Loading. Please wait...");
        }

        @Override
        protected JSONObject doInBackground(String... args) {
//            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

            try {
                MyDevice device = new MyDevice();

                HashMap<String, String> params = new HashMap<String, String>();
                params.put("macAddress", device.getMacAddress());
                params.put("ip", device.getIpAddress());
                params.put("typeDevice", device.getDeviceType());


                JSONObject json = jsonParser.makeHttpRequest(
                        WEB_API_URL, WEB_API_METHOD, params);

                if (json != null) {
                    return json;
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        protected void onPostExecute(JSONObject json) {
            if (getActivity().isDestroyed()) {
                return;
            }
            dismissProgressDialog();

            JSONArray buildingArray = null;

            buildingList = new ArrayList<Building>();
            buildingAdapter = new ArrayList<String>();

            if (json != null) {
//                Toast.makeText(getActivity(), json.toString(),
//                        Toast.LENGTH_LONG).show();
                clearFragmentSpinnerData();
                hideFragmentContent();

                if (buildingLabel != null)
                    buildingLabel.setVisibility(View.VISIBLE);
                if (spinnerBuilding != null)
                    spinnerBuilding.setVisibility(View.VISIBLE);

                try {
                    // workaround, add empty building at the top of the list
                    Building building = new Building();

                    // add building to the list of buildings
                    building.setId(0);
                    building.setName("Select building...");
                    buildingList.add(building);
                    // populate spinner with building names
                    buildingAdapter.add("Select building...");

                    UNIQUE_ID = json.getString(TAG_UNIC_ID);

                    buildingArray = json.getJSONArray(TAG_BUILDING);
                    for (int i = 0; i < buildingArray.length(); i++) {
                        JSONObject jsonobject = buildingArray.getJSONObject(i);

                        building = new Building();

                        // add building to the list of buildings
                        building.setId(jsonobject.getInt("id"));
                        building.setName(jsonobject.getString("name"));
                        buildingList.add(building);

                        // populate spinner with building names
                        buildingAdapter.add(jsonobject.getString("name"));
                    }

                    spinnerBuilding.setAdapter(new ArrayAdapter<String>(getActivity(),
                                    android.R.layout.simple_spinner_dropdown_item,
                                    buildingAdapter));
                    spinnerBuilding.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                        @Override
                        public void onItemSelected(AdapterView<?> arg0,
                                                   View arg1, int position, long arg3) {
                            // TODO Auto-generated method stub
                            if (position == 0)
                                return;
//                            Toast.makeText(getActivity(), buildingList.get(position).getId() + " " + buildingList.get(position).getName(),
//                                    Toast.LENGTH_LONG).show();

//                            new FloorPostAsync().execute(buildingList.get(position));

                            if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                                new FloorPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, buildingList.get(position));
                            else
                                new FloorPostAsync().execute(buildingList.get(position));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                            // TODO Auto-generated method stub
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(), "NULL RESPONSE",
                        Toast.LENGTH_LONG).show();
            }
        }

    }

    private void clearPastBuildingSpinnerData() {
        if (floorList != null && !floorList.isEmpty()) {
            floorList.clear();
            spinnerFloor.setAdapter(null);
        }
        if (roomList != null && !roomList.isEmpty()) {
            roomList.clear();
            spinnerRoom.setAdapter(null);
        }
        if (deviceList != null && !deviceList.isEmpty()) {
            deviceList.clear();
            spinnerDevice.setAdapter(null);
        }
        if (patientList != null && !patientList.isEmpty()) {
            patientList.clear();
            spinnerPatient.setAdapter(null);
        }
    }

    private void clearPastFloorSpinnerData() {
        if (roomList != null && !roomList.isEmpty()) {
            roomList.clear();
            spinnerRoom.setAdapter(null);
        }
        if (deviceList != null && !deviceList.isEmpty()) {
            deviceList.clear();
            spinnerDevice.setAdapter(null);
        }
        if (patientList != null && !patientList.isEmpty()) {
            patientList.clear();
            spinnerPatient.setAdapter(null);
        }
    }

    private void clearPastRoomSpinnerData() {
        if (deviceList != null && !deviceList.isEmpty()) {
            deviceList.clear();
            spinnerDevice.setAdapter(null);
        }
        if (patientList != null && !patientList.isEmpty()) {
            patientList.clear();
            spinnerPatient.setAdapter(null);
        }
    }

    private void clearFragmentSpinnerData() {
        if (buildingList != null && !buildingList.isEmpty()) {
            buildingList.clear();
            spinnerBuilding.setAdapter(null);
        }
        if (floorList != null && !floorList.isEmpty()) {
            floorList.clear();
            spinnerFloor.setAdapter(null);
        }
        if (roomList != null && !roomList.isEmpty()) {
            roomList.clear();
            spinnerRoom.setAdapter(null);
        }
        if (deviceList != null && !deviceList.isEmpty()) {
            deviceList.clear();
            spinnerDevice.setAdapter(null);
        }
        if (patientList != null && !patientList.isEmpty()) {
            patientList.clear();
            spinnerPatient.setAdapter(null);
        }
    }

    private void hideFragmentContent() {
        if (buttonAddPatient != null)
            buttonAddPatient.setVisibility(View.GONE);
        if (buttonAddDevice != null)
            buttonAddDevice.setVisibility(View.GONE);

        if (buildingLabel != null)
            buildingLabel.setVisibility(View.GONE);
        if (floorLabel != null)
            floorLabel.setVisibility(View.GONE);
        if (roomLabel != null)
            roomLabel.setVisibility(View.GONE);
        if (deviceLabel != null)
            deviceLabel.setVisibility(View.GONE);
        if (patientLabel != null)
            patientLabel.setVisibility(View.GONE);

        if (spinnerBuilding != null)
            spinnerBuilding.setVisibility(View.GONE);
        if (spinnerFloor != null)
            spinnerFloor.setVisibility(View.GONE);
        if (spinnerRoom != null)
            spinnerRoom.setVisibility(View.GONE);
        if (spinnerDevice != null)
            spinnerDevice.setVisibility(View.GONE);
        if (spinnerPatient != null)
            spinnerPatient.setVisibility(View.GONE);
    }

    private void hidePastBuildingContent() {
        if (buttonAddPatient != null)
            buttonAddPatient.setVisibility(View.GONE);
        if (buttonAddDevice != null)
            buttonAddDevice.setVisibility(View.GONE);

        if (floorLabel != null)
            floorLabel.setVisibility(View.GONE);
        if (roomLabel != null)
            roomLabel.setVisibility(View.GONE);
        if (deviceLabel != null)
            deviceLabel.setVisibility(View.GONE);
        if (patientLabel != null)
            patientLabel.setVisibility(View.GONE);

        if (spinnerFloor != null)
            spinnerFloor.setVisibility(View.GONE);
        if (spinnerRoom != null)
            spinnerRoom.setVisibility(View.GONE);
        if (spinnerDevice != null)
            spinnerDevice.setVisibility(View.GONE);
        if (spinnerPatient != null)
            spinnerPatient.setVisibility(View.GONE);
    }

    private void hidePastFloorContent() {
        if (buttonAddPatient != null)
            buttonAddPatient.setVisibility(View.GONE);
        if (buttonAddDevice != null)
            buttonAddDevice.setVisibility(View.GONE);

        if (roomLabel != null)
            roomLabel.setVisibility(View.GONE);
        if (deviceLabel != null)
            deviceLabel.setVisibility(View.GONE);
        if (patientLabel != null)
            patientLabel.setVisibility(View.GONE);

        if (spinnerRoom != null)
            spinnerRoom.setVisibility(View.GONE);
        if (spinnerDevice != null)
            spinnerDevice.setVisibility(View.GONE);
        if (spinnerPatient != null)
            spinnerPatient.setVisibility(View.GONE);
    }

    private void hidePastRoomContent() {
        if (buttonAddPatient != null)
            buttonAddPatient.setVisibility(View.GONE);
        if (buttonAddDevice != null)
            buttonAddDevice.setVisibility(View.GONE);

        if (deviceLabel != null)
            deviceLabel.setVisibility(View.GONE);
        if (patientLabel != null)
            patientLabel.setVisibility(View.GONE);

        if (spinnerDevice != null)
            spinnerDevice.setVisibility(View.GONE);
        if (spinnerPatient != null)
            spinnerPatient.setVisibility(View.GONE);
    }

    private DisplayMetrics getWindowDescription() {
        DisplayMetrics metrics = getActivity().getApplicationContext().getResources().getDisplayMetrics();
//        DisplayMetrics metrics = new DisplayMetrics();
//        getActivity().getApplicationContext().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        return metrics;
    }

    private void initPopupDimensions(DisplayMetrics metrics) {
        popupWidth = metrics.widthPixels - OFFSET_X;
        popupHeight = metrics.heightPixels - OFFSET_Y;
        popupUserImageWidth = popupWidth/3;
        popupUserImageHeight = popupHeight/3;
    }

//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.provisioning_ems_refresh_view_key:
//                clearFragmentSpinnerData();
//                hideFragmentContent();
////                new BuildingPostAsync().execute(WEB_API_URL, WEB_API_METHOD);
//                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
//                    new BuildingPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WEB_API_URL, WEB_API_METHOD);
//                else
//                    new BuildingPostAsync().execute(WEB_API_URL, WEB_API_METHOD);
//                break;
//            case R.id.provisioning_ems_add_device_key:
////                if (p != null)
//                showPopupDevice(getActivity(), p, null);
//                break;
//            case R.id.provisioning_ems_add_patient_key:
////                if (p != null)
//                showPopupPatient(getActivity(), p, null);
//                break;
//        }
//    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
//        new BuildingPostAsync().execute(WEB_API_URL, WEB_API_METHOD);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
            new BuildingPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WEB_API_URL, WEB_API_METHOD);
        else
            new BuildingPostAsync().execute(WEB_API_URL, WEB_API_METHOD);

        DisplayMetrics metrics = getWindowDescription();
        initPopupDimensions(metrics);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.provisioning_ems, container, false);

        buttonRefreshView = (Button) view.findViewById(R.id.provisioning_ems_refresh_view_key);
        buttonRefreshView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFragmentSpinnerData();
                hideFragmentContent();
//                new BuildingPostAsync().execute(WEB_API_URL, WEB_API_METHOD);
                if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.HONEYCOMB)
                    new BuildingPostAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, WEB_API_URL, WEB_API_METHOD);
                else
                    new BuildingPostAsync().execute(WEB_API_URL, WEB_API_METHOD);
            }
        });
//        buttonRefreshView.setOnClickListener(this);

        buttonAddDevice = (Button) view.findViewById(R.id.provisioning_ems_add_device_key);
        buttonAddDevice.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupDevice(getActivity(), p, null);
            }
        });
//        buttonAddDevice.setOnClickListener(this);

        buttonAddPatient = (Button) view.findViewById(R.id.provisioning_ems_add_patient_key);
        buttonAddPatient.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupPatient(getActivity(), p, null);
            }
        });
//        buttonAddPatient.setOnClickListener(this);

        buildingLabel = (TextView) view.findViewById(R.id.provisioning_ems_building_list_text_key);
        floorLabel = (TextView) view.findViewById(R.id.provisioning_ems_floor_list_text_key);
        roomLabel = (TextView) view.findViewById(R.id.provisioning_ems_room_list_text_key);
        deviceLabel = (TextView) view.findViewById(R.id.provisioning_ems_device_list_text_key);
        patientLabel = (TextView) view.findViewById(R.id.provisioning_ems_patient_list_text_key);

        spinnerBuilding = (Spinner) view.findViewById(R.id.provisioning_ems_building_list_spinner_key);
        spinnerFloor = (Spinner) view.findViewById(R.id.provisioning_ems_floor_list_spinner_key);
        spinnerRoom = (Spinner) view.findViewById(R.id.provisioning_ems_room_list_spinner_key);
        spinnerPatient = (Spinner) view.findViewById(R.id.provisioning_ems_patient_list_spinner_key);
        spinnerDevice = (Spinner) view.findViewById(R.id.provisioning_ems_device_list_spinner_key);

        hideFragmentContent();

//        new BuildingPostAsync().execute(WEB_API_URL, WEB_API_METHOD);

        return view;
    }

}
