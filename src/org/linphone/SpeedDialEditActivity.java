package org.linphone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.content.pm.ActivityInfo;

public class SpeedDialEditActivity extends Activity {
    public static final String EXT_ARG_PATH = "path";
    public static final String EXT_ARG_TEXT = "data";
    private String data;
    private String path;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Hack to avoid to draw twice LinphoneActivity on tablets
        if (getResources().getBoolean(R.bool.orientation_portrait_only)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.activity_speed_dial_edit);

        Intent intent = getIntent();

        this.path = intent.getStringExtra(EXT_ARG_PATH);
        this.data = intent.getStringExtra(EXT_ARG_TEXT);
    }
}
