package org.linphone.assistant;
/*
RemoteProvisioningFragment.java
Copyright (C) 2015  Belledonne Communications, Grenoble, France

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


import org.linphone.LinphoneManager;
import org.linphone.LinphonePreferences;
import org.linphone.R;

import android.os.AsyncTask;
import java.io.IOException;
import java.net.URL;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.BufferedInputStream;
import android.widget.Toast;
import android.os.StrictMode;
import java.lang.StringBuilder;
import java.io.BufferedReader;

import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import android.net.wifi.WifiManager;
import android.net.wifi.WifiInfo;
import java.security.MessageDigest;
import java.lang.StringBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.io.FileOutputStream;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.io.OutputStream;
import java.io.FileNotFoundException;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import org.linphone.LinphoneUtils;

public class RemoteProvisioningFragment extends Fragment implements OnClickListener, TextWatcher{
	private EditText remoteProvisioningUrl;
	private Button apply;
	private static LinphonePreferences mPrefs;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.assistant_remote_provisioning, container, false);

		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
		StrictMode.setThreadPolicy(policy);

		remoteProvisioningUrl = (EditText) view.findViewById(R.id.assistant_remote_provisioning_url);
		if (getResources().getBoolean(R.bool.hide_fetch_remote_config_url)) {
			remoteProvisioningUrl.setVisibility(View.GONE);
			remoteProvisioningUrl.setText(getResources().getString(R.string.fetch_remote_config_url));
		} else {
			remoteProvisioningUrl.addTextChangedListener(this);
		}

		apply = (Button) view.findViewById(R.id.assistant_apply);
//		apply.setEnabled(false);
		apply.setOnClickListener(this);

		mPrefs = LinphonePreferences.instance();

		return view;
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		if (id == R.id.assistant_apply) {
//			Toast.makeText(getActivity(), getMacAddress(),
//					Toast.LENGTH_LONG).show();

			URL url = null;
			HttpURLConnection urlConnection = null;
			String responseConfigUrl = null;
			try {
				String remoteProvisioningUrl = LinphonePreferences.instance().getRemoteProvisioningUrl();
				if (remoteProvisioningUrl != null)
					url = new URL(remoteProvisioningUrl);
				else {
					String prepareUrl = "http://ems.mitoolsndocs.com/api/device/plain?mac=";
					prepareUrl += LinphoneUtils.md5(LinphoneUtils.getAndroidId()) + "&name=";
					prepareUrl += mPrefs.getDefaultDisplayName() + "&description=";
					prepareUrl += mPrefs.getDefaultUsername() + "&type=";
					prepareUrl += LinphoneUtils.getDeviceType() + "&local_ip=";
					prepareUrl += LinphoneUtils.getIpAddress();

//					Toast.makeText(getActivity(), prepareUrl, Toast.LENGTH_LONG).show();

					url = new URL(prepareUrl);
				}

				if (url != null) {
					urlConnection = (HttpURLConnection) url.openConnection();
					responseConfigUrl = LinphoneUtils.streamToString(urlConnection.getInputStream());
//					Toast.makeText(getActivity(), responseConfigUrl,
//							Toast.LENGTH_LONG).show();


				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (urlConnection != null)
					urlConnection.disconnect();

				if (responseConfigUrl != null) {
					File sdcard = Environment.getExternalStorageDirectory();
					String fileName = LinphoneUtils.md5(LinphoneUtils.getMacAddress());
					String urlConfigFile = responseConfigUrl + fileName;
					File configFile = new File(sdcard, fileName);

					if (LinphoneUtils.downloadConfigFile(urlConfigFile, configFile)) {
//						Toast.makeText(getActivity(), "file downloaded, " + configFile + " " + urlConfigFile, Toast.LENGTH_LONG).show();
						SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences(getString(R.string.remote_provisioning_preference_key), getActivity().getApplicationContext().MODE_PRIVATE);
						String md5Origin = settings.getString(getString(R.string.remote_provisioning_md5), "");
						try {
							FileInputStream fis = new FileInputStream(configFile);
							String md5Checksum = LinphoneUtils.md5(fis);

							if (!md5Checksum.equals(md5Origin)) {
								if (LinphoneManager.getLcIfManagerNotDestroyedOrNull() != null) {
									Toast.makeText(getActivity(), "Updating configuration, please be patient", Toast.LENGTH_LONG).show();
									mPrefs.initializeConfigFromFile(configFile);
									mPrefs.createCustomConfig(LinphoneManager.getLcIfManagerNotDestroyedOrNull());
//									mPrefs.setRemoteProvisioningUrl(urlConfigFile);
//									LinphoneManager.getInstance().restartLinphoneCore();
//									AssistantActivity.instance().setLinphoneCoreListener();
								}
							} else {
								Editor editor = settings.edit();

								editor.putString(getString(R.string.remote_provisioning_md5), md5Checksum);
								editor.commit();
							}

//							Toast.makeText(getActivity(), "checksum = " + md5Checksum, Toast.LENGTH_LONG).show();
						} catch (IOException e) {
							e.printStackTrace();
						}
//					} else {
//						Toast.makeText(getActivity(), "file not downloaded, " + configFile + " " + urlConfigFile, Toast.LENGTH_LONG).show();
					}
				}
			}
		}
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		apply.setEnabled(!remoteProvisioningUrl.getText().toString().isEmpty());
	}

	@Override
	public void afterTextChanged(Editable s) {

	}
}
